﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class QuestionsPage : MonoBehaviour // метода для заполнения страницы с вопросом и ответами
{
    [SerializeField] private Text QuestionText; // поле для текст вопроса
    [SerializeField] private Text QuestionText2; // поле для текст вопроса
    [SerializeField] private Transform spawnBtnParent; // место где будут создоватся кнопки может 2 может 5
    [SerializeField] private AnswerButton answerButton; // сам кнопка - префаб (готовый обьект)

    private void ClearParent() // чистим все  лишние кнопки со страницы (каждый раз могут быть новые)
    {
        int count = spawnBtnParent.childCount; // высчитываем сколько есть кнопок
        for (int i = 0; i < count; i++)
        {
            GameObject button = spawnBtnParent.GetChild(i).gameObject; // находим саму кнопку 
            Destroy(button); // удаляем ее
        }

        EkzamenManager.Instance.summ = 10;
    }

    public void Init(ItemEkzamen itemEkzamen) // метод для иницилизации страницы с вопросами ответами
    {
        QuestionText.text = itemEkzamen.question; // заполняем вопрос
        ClearParent(); // чистим все кнопки
        CreateButton(itemEkzamen); // создаем нужные кнопки с ответами 
    }

    public void CreateButton(ItemEkzamen itemEkzamen) // созздание кнопок с ответами
    {
        AnswerButton answerButtonReal = Instantiate(answerButton, spawnBtnParent); //Instantiate это метод создания обьекта нужно 1 параметр сам обьект 2й место где его создать
        answerButtonReal.Init(itemEkzamen.answerReal, 0, itemEkzamen.id);  // после создания обращаемся к этому созданому обьекту (кнопка) и заполняем ее отетами далее по такому же принципу
        AnswerButton answerButtonFake1 = Instantiate(answerButton, spawnBtnParent);
        answerButtonFake1.Init(itemEkzamen.fakeAnswer1, 1, itemEkzamen.id);
        
        if (itemEkzamen.fakeAnswer3 != "none") // проверяем если приходит "none" из айтема, а это тоже что в таблице, то пропускаем - (запистаь все кроме none) 
        {
            AnswerButton answerButtonFake2 = Instantiate(answerButton, spawnBtnParent);
            answerButtonFake2.Init(itemEkzamen.fakeAnswer2, 2, itemEkzamen.id);
        }
        
        if (itemEkzamen.fakeAnswer3 != "none")
        {
            AnswerButton answerButtonFake3 = Instantiate(answerButton, spawnBtnParent);
            answerButtonFake3.Init(itemEkzamen.fakeAnswer3, 3, itemEkzamen.id);
        }
        
        if (itemEkzamen.fakeAnswer4 != "none")
        {
            AnswerButton answerButtonFake4 = Instantiate(answerButton, spawnBtnParent);
            answerButtonFake4.Init(itemEkzamen.fakeAnswer4, 4, itemEkzamen.id);
        }
        
        if (itemEkzamen.fakeAnswer5 != "none")
        {
            AnswerButton answerButtonFake5 = Instantiate(answerButton, spawnBtnParent);
            answerButtonFake5.Init(itemEkzamen.fakeAnswer5, 5, itemEkzamen.id);
        }
        
        // int count = spawnBtnParent.childCount; // высчитываем сколько есть кнопок
        // for (int i = 0; i < count; i++) // проходимся по всем кнопкам
        // {
        //     Transform button = spawnBtnParent.GetChild(i);
        //
        //     if (FivtyByFivty() == 1) // получаем рандом 50  на 50
        //     {
        //         button.SetAsLastSibling(); // если 1 то ставми кнопку последней
        //     }
        //     else
        //     {
        //         button.SetAsLastSibling(); // если 0 то первой
        //     }
        // }
    }

    // private int FivtyByFivty() //получить рандом 50 на 50
    // {
    //     return Random.Range(0, 1);
    // }

    public void PressAnswerBtn()
    {
        
    }
}
