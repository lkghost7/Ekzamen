﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class ShowData : MonoBehaviour
{
    [SerializeField] private GameObject startMain; // это вся стартовая страница, ка ктолько нажмем старт экзамен мы ее просто прячем
    
    public GetSheetData GetSheetData; // ссылка на обьект с которого мы обратимся к данным
    private List<ItemEkzamen> currentItemEkzamenList = new List<ItemEkzamen>(); // текущий набор для экзамена например из 300 воропсов тут каждый раз будет 10 случайных
    
    private void Start() // стартует при запуске приложения
    {
        StartCoroutine(Stt()); // запукает метод с задержкой
    }

    IEnumerator Stt()
    {
        yield return new WaitForSeconds(0.3f);
        View();
    }

    public void View() // метод вывода данных на экран
    {
        currentItemEkzamenList.Clear(); // обнуляем листо что бы положить снова 10 разных вопросов

        for (int i = 0; i < 10; i++) // проходимся 10 раз
        {
            currentItemEkzamenList.Add(GetSheetData.ItemEkzamensDictionary[Random.Range(1,25)]); // получаем случайное число от 1 до 25 (это пока хардкод но сейчас всего 25 вопросв)
        }

        for (int i = 0; i < 10; i++) 
        {
            EkzamenManager.Instance.questionsPages[i].Init(currentItemEkzamenList[i]); //иницилизируем данные, заполняем страницу на вход передается из листа айтем экзамен 
        }

    }


    public void RestartBtn() // кнопка для рестарта что бы перегрухить все снова, только для дебага и что каждый раз пока что не перезапускать программу
    {
        for (int i = 0; i < 10; i++)
        {
            EkzamenManager.Instance.questionsPages[i].gameObject.SetActive(true); // включаем все 10 страниц
        }
        startMain.SetActive(true); //включаем стратовую страницу
   
        View();
    }
}
